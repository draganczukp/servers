#!/bin/sh

OUTPUT_FILE_PATH='/data/Data/modules/betterrolls5e/scripts/custom-roll.js'

PATCHED_FILE_URL='https://raw.githubusercontent.com/RedReign/FoundryVTT-BetterRolls5e/7f270bfefca2173961a39e6307f05690a144abe4/betterrolls5e/scripts/custom-roll.js'

if [[ ! -e "$OUTPUT_FILE_PATH" ]]; then
	log "Output file not found. Assuming BetterRolls5e not installed and exiting"
	exit 0
fi

log "Backing up original file"
cp "$OUTPUT_FILE_PATH" "${OUTPUT_FILE_PATH}.bak"

log "Downloading fixed file"
wget "$PATCHED_FILE_URL" -O "$OUTPUT_FILE_PATH"

if [[ "$?" -eq "0" ]]; then
	log "BetterRolls should now be fixed"
else
	log_error "Patching failed. Check the logs and restore the backup"
fi

