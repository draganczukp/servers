#!/bin/sh

# Path to the CSS file
SOURCE="/patches/login-screen.css"

# Which file to patch
DEST="/home/foundry/resources/app/public/css/style.css"

# Temporary working directory
TMP_DIR=$(mktemp -d)

# Cleanup in case the script gets stopped prematurely
trap "rm -rf $TMP_DIR" 1 2 3 9 15

log "Adding custom CSS..."
log "$SOURCE -> $DEST"

# This is the important part. This command prints the
# original file ($DEST) and the CSS patch ($SOURCE), then
# it saves the output to a temporary file
cat $DEST $SOURCE > $TMP_DIR/style.css

log "Replacing file..."

# Once the temp file is ready, we need to move replace
# old file with the new and patched file
cp $TMP_DIR/style.css $DEST

log "Should be working now"

# Cleanup
rm -rf $TMP_DIR